use std::io::{BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn main() {
    println!("Exemple value :{}",part_a("./sample"))}
    
fn part_a(input:&str) -> isize {
    let reader = BufReader::new(File::open("./input").expect("Couldn't load the input values"));
    let mut data:HashMap<usize,u32> = HashMap::new();
    
    for value in reader.lines() {
        let line = value.unwrap();
        for (rank,bit) in line.chars().enumerate() {
            if data.contains_key(&rank) {data[&rank] += bit.to_digit(10).unwrap()}
            else {data.insert(rank,bit.to_digit(10).unwrap())}
        }
    }
    
    let mut delta_vec:<bool> = Vec::new();

    for ones_in_i in data.enumerate() {
        delta_vec.push(ones_in_i <= reader.lines().count()/2)}
}

mod tests {
    use super::*;

    #[test]
    fn sample_part_a() {
        assert_eq!(part_a("./sample"), 198)
    }
}