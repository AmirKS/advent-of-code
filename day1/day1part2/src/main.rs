use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let reader = BufReader::new(File::open("../../../input").expect("Couldn't load the input values"));
    let mut prev_value = 0;
    let mut prev_prev_value = 0;
    let mut prev_temp_sum = 0;
    let mut temp_sum = 0;
    let mut m = -3;
    for value in reader.lines() {
        let line: String = value.unwrap();
        let actual_value: i32 = line.parse().unwrap();
        temp_sum = prev_value + prev_prev_value + actual_value;
        
        if temp_sum > prev_temp_sum {
            m+=1
        }

        prev_prev_value = prev_value;
        prev_value =  actual_value;
        prev_temp_sum = temp_sum;
    }
        println!("{}",m)
    }