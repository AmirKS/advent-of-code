use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let reader = BufReader::new(File::open("../input").expect("Couldn't load the input values"));
    let mut prev_value =  0;
    let mut m = -1;
    for value in reader.lines() {
        let line: String = value.unwrap();
        let actual_value: i32 = line.parse().unwrap();
        if actual_value > prev_value {
            m+=1
        }
        /* let //va planter */ prev_value =  actual_value;
    }
        println!("{}",m)
    }