use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let reader = BufReader::new(File::open("./input").expect("Couldn't load the input values"));
    let mut x_value = 0;
    let mut y_value = 0;

    for value in reader.lines() {
        let line = value.unwrap();
        let (instruction,distance_str) = line.split_once(' ').unwrap();

        let distance :i32 = distance_str.parse().unwrap();

        let good: (i32,i32) = match instruction {
            "forward"   => (1,0),
            "up"        => (0,-1),
            "down"      => (0,1),
            _ => (0,0)
        };

        x_value += good.0*distance;
        y_value += good.1*distance;
    }
    println!("{}",x_value*y_value)
}