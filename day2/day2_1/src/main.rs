use std::io::{BufRead, BufReader};
use std::collections::{HashMap};
use std::fs::File;

fn main() {
    let reader = BufReader::new(File::open("./input").expect("Couldn't load the input values"));
    let mut position:HashMap<&str,i32> = HashMap::new();

    for value in reader.lines() {
        let line = value.unwrap();
        let mut split = line.split_whitespace();

        let (instruction_str,distance_str) = (split.next().unwrap(), split.next().unwrap());

        let distance = distance_str.to_string().parse().unwrap();
        /*let instruction = instruction_str.to_string();
*/
        position.entry(instruction_str).or_insert(distance);
        position[instruction] += distance;
    }

    for (key, value) in &position {
            println!("{}: {}", key, value);
    }
}